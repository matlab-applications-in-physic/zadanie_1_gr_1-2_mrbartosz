%polecenie 1 h/(2?)
 h = 6.62607015E-34 %establishing plancks constant
 Polecenie1 = h/2*pi     %calculating the value of the equation
 
 %polecenie 2 sin(30°/e)
 e=exp(1) %constant e
 Polecenie2 = sind(30/e) %sind works on degrees while sin works on radians, calculating the value of the equation
 
 %polecenie 3 0x00123d3 / (2,455·1023)
 dec=hex2dec('00123d3') %function hex2dec turns hexadecimal numbers into decimal
 Polecenie3 = dec/2,455E23 %calculating the value of the equation 
 
 %polecenie 4 ?e??
 e=exp(1) %constant e
 Polecenie4 = sqrt(e-pi) %function sqrt calculates square root
 
 %polecenie 5 Wyświetlić 10. miejsce po przecinku liczby ?
 //polecenie 5 (works in scilab)
format(20)    //estabilishing the number of digits showing on the screen
x = %pi*1E10   //multiplying Pi by 1E10 to have its 10th digit after the decimal point shown
y = fix(x)   //rounding the number to have an integer 
z = y/10      //dividing the number over 10 to get 1 digit after decimal point  
c = fix(z)  //rounding the number again to get an integer
v = c*10    //multiplying by 10 to get number in the same power as "y"
Polecenie5 =  y-v    //calculating the value. The result is Pi's 10th number after the decimal point
 
 %polecenie 6 Ile dni upłynęło o daty Twoich urodzin?
 //polecenie 6 (works in scilab)
t1=[1999 08 18 11 00 13.535] //my birthday
t2=clock()  //actual date
Polecenie6 = E1=etime(t2(),t1)/(3600 * 24) //calculating the value, the result is time that has passed since I was born
 // error in SciLab:   Error: syntax error, unexpected =, expecting end of file
 
 %polecenie 7 arctg
 liczba=hex2dec('aabb') %turning hexadecimal number into decimal
 R=6371 %establishing Earths radius in km
 Polecenie7 = atan((exp((sqrt(7)/2)-log(R/10E5)))/liczba) %calculating the value of the equation
 
 %polecenie 8  Obliczyć liczbę atomów w 1/5 mikromola alkoholu etylowego
 avogadr=6.02214076E23 %establishing avogadros constant
 atoms=9 %number of atoms in ethanol 
 
 Polecenie8 = (1/5)*atoms*10E-6*avogadr %calculating the value (number of atoms in 1/5 micro mol of ethanol)
 
%% last excercise is missing 